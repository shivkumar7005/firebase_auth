package com.firebase;

import com.firebase.controller.LoginController;

import javafx.application.Application;

public class Main {

    //launching the LoginController class
    public static void main(String[] args) {
        Application.launch(LoginController.class,args);
    }   
}
